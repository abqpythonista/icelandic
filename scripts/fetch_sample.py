from line_tools.merge import merge_content, file_content
import sys
import random

if (__name__ == "__main__"):
   count = int(sys.argv[2]) if (len(sys.argv)>2) else 100
   rr = [w for w in filter(lambda x: x["line"].find("https:")<0 , merge_content([file_content(f) for f in sys.argv[1:]]))]
   indices = [i for i in range(0,len(rr))]
   random.shuffle(indices)
   selected = sorted(indices[:count])
   rr = [rr[i] for i in selected]
   for line in rr:
      print(line["line"])
