#!/usr/bin/python
import sys
import re
import pprint
from functools import reduce

def biggest_line(x,y):
    if (len(x.get("line","")) > len(y.get("line",""))):
        return x.get("line","")
    else:
        return y.get("line","")

def biggest_examples(x,y):
    if (len(x.get("examples", [])) > len(y.get("examples", []))):
        return x.get("examples", [])
    else:
        return y.get("examples", [])

def line_mashup(k, collections):
    candidates = [c.get(k, {}) for c in collections]
    #print(candidates)
    return reduce(
        lambda x,y:
        {
            "word" : k,
            "line": biggest_line(x,y),
            "examples": biggest_examples(x,y)
        },
        candidates,
        {}
    )

def merge_content(f):
  collections = [c for c in f]
  keys = sorted(list(reduce(lambda x,y: x.union(y), [t.keys() for t in collections],  set())))
  return [line_mashup(k, collections) for k in keys]

def line_key(line):
    if (line.find(",") < 0):
        return line.strip()
    return line[:line.find(",")].strip()

def file_content(fileName):
    example = dict()
    print(fileName)
    with open(fileName, 'r') as input:
        key = ""
        for line in input.readlines():
            if (re.match("^\s",line)):
                example[key]["examples"] = example.get("examples", []) + [line.strip()]
            else:
                key = line_key(line)
                example[key] = dict()
                example[key]["line"] = line.strip()
                example[key]["word"] = key
    return example

if (__name__ == "__main__"):
   rr = merge_content([file_content(f) for f in sys.argv[1:]])
   for line in rr:
       print(line.get('line', ''))

