#!/usr/bin/python
import random
import sys
def load_lines(filename):
  with open(filename,'r') as input:
    return input.readlines()

def unread(line):
  return line.find("http://")<0

def random_line(words, screen=unread, count=10):
  selected = [w for w in words if screen(w)]
  random.shuffle(selected)
  return selected[:count]


  
if (__name__)=="__main__":
  print("".join(random_line(load_lines(sys.argv[1]), unread, 100)))
